<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<UiMod name="Vertigo" version="1.2" date="16/4/2009" >

		<Author name="Aiiane" email="aiiane@aiiane.net" />
		<Description text="Lets you customize the format of action bars." />
        
        <VersionSettings gameVersion="1.2.1" />
        
        <Dependencies>
            <Dependency name="LibSlash" />
        </Dependencies>
        
		<Files>
			<File name="Vertigo.lua" />
		</Files>
		
		<OnInitialize>
            <CallFunction name="Vertigo.Initialize" />
		</OnInitialize>
		<OnUpdate/>
		<OnShutdown/>
		
	</UiMod>
</ModuleFile>
