Vertigo = {}

function Vertigo.Initialize()
    LibSlash.RegisterSlashCmd("vert", function(input) Vertigo.SlashHandler(input) end)
    LibSlash.RegisterSlashCmd("vertigo", function(input) Vertigo.SlashHandler(input) end)
end

function Vertigo.SlashHandler(args)
    local opt, val = args:match("([a-z0-9]+)[ ]?(.*)")
    
    if opt == "cols" then Vertigo.SetBarCols(val)
    elseif opt == "back" then Vertigo.SetBarBack(val)
    elseif opt == "pager" then Vertigo.SetBarPager(val)
    elseif opt == "empty" then Vertigo.SetBarEmpty(val)
    elseif opt == "buttons" then Vertigo.SetBarButtons(val)
    else
        EA_ChatWindow.Print(L"Available commands for /vertigo: cols, back, pager, empty, buttons.")
    end
end

function Vertigo.SetBarCols(val)
    local barNum, colNum = val:match("([0-9]+)[^0-9]([0-9]+)")
    if not barNum then EA_ChatWindow.Print(L"Usage: /vertigo cols [bar#] [cols#] - Sets the number of columns for a bar.") return end
    
    barNum = tonumber(barNum)
    colNum = tonumber(colNum)
    if not ActionBars.m_Bars[barNum] then
        EA_ChatWindow.Print(L"Bar number "..towstring(barNum)..L" does not exist.")
    else
        if colNum < 1 or colNum > 12 then
            EA_ChatWindow.Print(L"The number of columns must be somewhere from 1 to 12.")
        else
            ActionBarClusterManager:UnregisterClusterWithLayoutEditor ()
            ActionBarClusterManager.m_Settings:SetActionBarSetting(barNum, "columns", colNum)
            ActionBars.m_Bars[barNum].m_ColumnCount = colNum
            if colNum < 12 then
                ActionBarClusterManager.m_Settings:SetActionBarSetting(barNum, "selector", ActionBarConstants.HIDE_PAGE_SELECTOR)
                ActionBars.m_Bars[barNum].m_Background:Show(false)
            end
            ActionBars.m_Bars[barNum]:AnchorButtons()
            ActionBarClusterManager:ReanchorCluster()
            ActionBarClusterManager:RegisterClusterWithLayoutEditor()
            EA_ChatWindow.Print(L"Reloading UI to apply settings...")
            InterfaceCore.ReloadUI()
        end
    end
end
                
function Vertigo.SetBarBack(val)
    local barNum, op = val:match("([0-9]+) ([a-z]+)")
    if not barNum then EA_ChatWindow.Print(L"Usage: /vertigo back [bar#] [show|hide] - shows or hides the bar background.") return end
    
    barNum = tonumber(barNum)
    if not ActionBars.m_Bars[barNum] then
        EA_ChatWindow.Print(L"Bar number "..towstring(barNum)..L" does not exist.")
    else
        if op ~= "hide" and op~="show" then
            EA_ChatWindow.Print(L"Usage: /vertigo back [bar#] [show|hide]")
        else
            if op == "hide" then
                ActionBarClusterManager.m_Settings:SetActionBarSetting(barNum, "background", ActionBarConstants.HIDE_BACKGROUND)
                ActionBars.m_Bars[barNum].m_ShowBackground = ActionBarConstants.HIDE_BACKGROUND
                ActionBars.m_Bars[barNum].m_Background:Show(false)
            else
                ActionBarClusterManager.m_Settings:SetActionBarSetting(barNum, "background", ActionBarConstants.SHOW_BACKGROUND)
                ActionBars.m_Bars[barNum].m_ShowBackground = ActionBarConstants.SHOW_BACKGROUND
                ActionBars.m_Bars[barNum].m_Background:Show(true)
            end
        end
    end
end

function Vertigo.SetBarPager(val)
    local barNum, op = val:match("([0-9]+) ([a-z]+)")
    if not barNum then EA_ChatWindow.Print(L"Usage: /vertigo pager [bar#] [left|right|hide] - changes the position or hides the page selector.") return end
    
    barNum = tonumber(barNum)
    if not ActionBars.m_Bars[barNum] then
        EA_ChatWindow.Print(L"Bar number "..towstring(barNum)..L" does not exist.")
    else
        if op ~= "hide" and op~="left" and op~="right" then
            EA_ChatWindow.Print(L"Usage: /vertigo pager [bar#] [left|right|hide]")
        else
            ActionBarClusterManager:UnregisterClusterWithLayoutEditor ()
            if op == "hide" then
                ActionBarClusterManager.m_Settings:SetActionBarSetting(barNum, "selector", ActionBarConstants.HIDE_PAGE_SELECTOR)
                ActionBars.m_Bars[barNum].m_PageSelectorMode = ActionBarConstants.HIDE_PAGE_SELECTOR
                if ActionBars.m_Bars[barNum].m_PageSelectorWindow then
                    ActionBars.m_Bars[barNum].m_PageSelectorWindow:Show(false)
                end
            elseif op == "left" then
                ActionBarClusterManager.m_Settings:SetActionBarSetting(barNum, "selector", ActionBarConstants.SHOW_PAGE_SELECTOR_LEFT)
                ActionBars.m_Bars[barNum].m_PageSelectorMode = ActionBarConstants.SHOW_PAGE_SELECTOR_LEFT
                if ActionBars.m_Bars[barNum].m_PageSelectorWindow then
                    ActionBars.m_Bars[barNum].m_PageSelectorWindow:Show(true)
                else
                    EA_ChatWindow.Print(L"Reloading UI to apply settings...")
                    InterfaceCore.ReloadUI()
                end
            elseif op == "right" then
                ActionBarClusterManager.m_Settings:SetActionBarSetting(barNum, "selector", ActionBarConstants.SHOW_PAGE_SELECTOR_RIGHT)
                ActionBars.m_Bars[barNum].m_PageSelectorMode = ActionBarConstants.SHOW_PAGE_SELECTOR_RIGHT
                if ActionBars.m_Bars[barNum].m_PageSelectorWindow then
                    ActionBars.m_Bars[barNum].m_PageSelectorWindow:Show(true)
                else
                    EA_ChatWindow.Print(L"Reloading UI to apply settings...")
                    InterfaceCore.ReloadUI()
                end
            end
            ActionBars.m_Bars[barNum]:AnchorButtons()
            ActionBarClusterManager:ReanchorCluster()
            ActionBarClusterManager:RegisterClusterWithLayoutEditor()
        end
    end
end

function Vertigo.SetBarEmpty(val)
    local barNum, op = val:match("([0-9]+) ([a-z]+)")
    if not barNum then EA_ChatWindow.Print(L"Usage: /vertigo empty [bar#] [show|hide] - shows or hides empty buttons.") return end
    
    barNum = tonumber(barNum)
    if not ActionBars.m_Bars[barNum] then
        EA_ChatWindow.Print(L"Bar number "..towstring(barNum)..L" does not exist.")
    else
        if op ~= "hide" and op~="show" then
            EA_ChatWindow.Print(L"Usage: /vertigo empty [bar#] [show|hide]")
        else
            if op == "hide" then
                ActionBarClusterManager.m_Settings:SetActionBarSetting(barNum, "showEmptySlots", ActionBarConstants.HIDE_EMPTY_SLOTS)
                ActionBars.m_Bars[barNum].m_ShowEmptySlots = ActionBarConstants.HIDE_EMPTY_SLOTS
                ActionBars.m_Bars[barNum]:UpdateShownSlots()
            else
                ActionBarClusterManager.m_Settings:SetActionBarSetting(barNum, "showEmptySlots", ActionBarConstants.SHOW_EMPTY_SLOTS)
                ActionBars.m_Bars[barNum].m_ShowEmptySlots = ActionBarConstants.SHOW_EMPTY_SLOTS
                ActionBars.m_Bars[barNum]:UpdateShownSlots()
            end
        end
    end
end

function Vertigo.SetBarButtons(val)
    local barNum, buttonNum = val:match("([0-9]+)[^0-9]([0-9]+)")
    if not barNum then EA_ChatWindow.Print(L"Usage: /vertigo buttons [bar#] [cols#] - sets the number of buttons in a bar.") return end
    
    barNum = tonumber(barNum)
    buttonNum = tonumber(buttonNum)
    if not ActionBars.m_Bars[barNum] then
        EA_ChatWindow.Print(L"Bar number "..towstring(barNum)..L" does not exist.")
    else
        if buttonNum < 1 or buttonNum > 120 then
            EA_ChatWindow.Print(L"The number of buttons must be somewhere from 1 to 120.")
        else
            ActionBarClusterManager:UnregisterClusterWithLayoutEditor ()
            ActionBarClusterManager.m_Settings:SetActionBarSetting(barNum, "buttonCount", buttonNum)
            ActionBars.m_Bars[barNum].m_ButtonCount = buttonNum
            ActionBars.m_Bars[barNum]:AnchorButtons()
            ActionBarClusterManager:ReanchorCluster()
            ActionBarClusterManager:RegisterClusterWithLayoutEditor()
            EA_ChatWindow.Print(L"Reloading UI to apply settings...")
            InterfaceCore.ReloadUI()
        end
    end
end